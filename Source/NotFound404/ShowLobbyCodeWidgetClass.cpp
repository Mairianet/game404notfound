﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ShowLobbyCodeWidgetClass.h"

#include "SaveGameData.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

UShowLobbyCodeWidgetClass::UShowLobbyCodeWidgetClass(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UShowLobbyCodeWidgetClass::NativeConstruct()
{
	Super::NativeConstruct();
	USaveGameData* dataToLoad = Cast<USaveGameData>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	if (dataToLoad != nullptr && dataToLoad->Code != -1)
	{
		UE_LOG(LogTemp, Warning, TEXT("CODE LOBBY %d"), dataToLoad->Code);
		CodeLabel->SetText(FText::AsNumber(dataToLoad->Code));
	}
	else
	{
		CodeLabel->SetText(FText::FromString("Error..."));
	}
}