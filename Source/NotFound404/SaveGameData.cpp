// Fill out your copyright notice in the Description page of Project Settings.


#include "SaveGameData.h"

USaveGameData::USaveGameData()
{
	UserId = "";
	Code = 0;
	ServerId = 0;
}

FString USaveGameData::GenerateNewId()
{
	FGuid NewId = FGuid::NewGuid();
	FString Id = NewId.ToString();
	return Id;
}
