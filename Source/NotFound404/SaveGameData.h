// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SaveGameData.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API USaveGameData : public USaveGame
{
	GENERATED_BODY()

public:
	USaveGameData();
	FString GenerateNewId();

	UPROPERTY(VisibleAnywhere)
	FString UserId;

	UPROPERTY(VisibleAnywhere)
	int32 Code;

	UPROPERTY(VisibleAnywhere)
	int32 ServerId;
};
