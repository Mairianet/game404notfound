﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "EnterLobbyCodeWidgetClass.h"

#include "HttpModule.h"
#include "SaveGameData.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Interfaces/IHttpResponse.h"
#include "Kismet/GameplayStatics.h"

UEnterLobbyCodeWidgetClass::UEnterLobbyCodeWidgetClass(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UEnterLobbyCodeWidgetClass::NativeConstruct()
{
	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &UEnterLobbyCodeWidgetClass::OnStartGameButtonClicked);
	}
}

void UEnterLobbyCodeWidgetClass::OnStartGameButtonClicked()
{
	FString EnteredCode = "";
	
	if (CodeInput)
	{
		EnteredCode = CodeInput->GetText().ToString();
	}
	
	UE_LOG(LogTemp, Error, TEXT("Entered code: %s"), *EnteredCode);
	ConnectToServerRequest(FCString::Atoi(*EnteredCode));
}

void UEnterLobbyCodeWidgetClass::ConnectToServerRequest(int32 Code)
{
	USaveGameData* dataToLoad = Cast<USaveGameData>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	const FString UserId = dataToLoad->UserId;
	const FString PutURL = FString::Printf(TEXT("http://localhost:8181/api/v1/servers/connect/%d?userId=%s"), Code, *UserId);
	FHttpModule* Http = &FHttpModule::Get();
	const TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->SetURL(PutURL);
	Request->SetVerb("PUT");
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	Request->OnProcessRequestComplete().BindLambda([this, dataToLoad, Code](FHttpRequestPtr Request, FHttpResponsePtr Response, bool bSuccess)
	{
        if (bSuccess && Response.IsValid() && (Response->GetResponseCode() == EHttpResponseCodes::Ok || Response->GetResponseCode() == EHttpResponseCodes::Created))
		{
			const FString ResponseContent = Response->GetContentAsString();
			UE_LOG(LogTemp, Error, TEXT("PUT request successful! Response Content: %s"), *ResponseContent);
        	TSharedPtr<FJsonObject> JsonObject;
			TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(ResponseContent);
			
			if (FJsonSerializer::Deserialize(JsonReader, JsonObject))
			{
				int32 ServerId;
				if (JsonObject->TryGetNumberField("id", ServerId))
				{
					dataToLoad->Code = Code;
					dataToLoad->ServerId = ServerId;
					UE_LOG(LogTemp, Warning, TEXT("CODE %d"), dataToLoad->Code);
					UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
					
					FString LevelName = TEXT("ComicBook");
					UGameplayStatics::OpenLevel(GetWorld(), FName(*LevelName));
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("Failed to parse code from JSON"));
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("Failed to deserialize JSON"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("PUT request failed!"));
		}
	});

	Request->ProcessRequest();
}
