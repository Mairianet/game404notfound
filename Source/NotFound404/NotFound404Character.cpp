// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotFound404Character.h"
#include "NotFound404Projectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Engine/LocalPlayer.h"
#include "Kismet/GameplayStatics.h"
#include "SaveGameData.h"

#include "Misc/EngineVersion.h"
#include "Misc/EngineBuildSettings.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include <Windows.h>
#include <iostream>

#include "Serialization/BufferArchive.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// ANotFound404Character

ANotFound404Character::ANotFound404Character()
{
	// Character doesnt have a rifle at start
	bHasRifle = false;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->SetFieldOfView(FieldOfView);

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(0.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));

}

void ANotFound404Character::CreateSaveFile()
{
	USaveGameData* dataToSave = Cast<USaveGameData>(UGameplayStatics::CreateSaveGameObject(USaveGameData::StaticClass()));
	dataToSave->UserId = dataToSave->GenerateNewId();
	FString Id = dataToSave->UserId;
	UE_LOG(LogTemp, Warning, TEXT("Generated Id: %s"), *Id);
	UGameplayStatics::SaveGameToSlot(dataToSave, "ServerDataSavedSlot", 0);
}

void ANotFound404Character::LoadGame()
{
	USaveGameData* dataToLoad = Cast<USaveGameData>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	if (dataToLoad != nullptr)
	{
		FString Id = dataToLoad->UserId;
		UE_LOG(LogTemp, Warning, TEXT("Loaded Id: %s"), *Id);
	}
	else if (!UGameplayStatics::DoesSaveGameExist("ServerDataSavedSlot", 0))
	{
		CreateSaveFile();
	}
}

void DisplayInfo() {
	DISPLAY_DEVICE Device;
	ZeroMemory(&Device, sizeof(Device));
	Device.cb = sizeof(Device);

	DWORD devNum = 0; // ����� ���������� (0 - ������ ����������, 1 - ������ � �.�.)
	DWORD adapterIndex = 0; // ������ ��������

	// �������� ���������� � ���������� ����������
	if (EnumDisplayDevices(NULL, devNum, &Device, 0))
	{
		// �������� ���������� � ����������� ��������
		DEVMODE devMode;
		ZeroMemory(&devMode, sizeof(devMode));
		devMode.dmSize = sizeof(devMode);

		if (EnumDisplaySettings(Device.DeviceName, ENUM_CURRENT_SETTINGS, &devMode))
		{
			// std::cout << "GPU Clock: " << devMode.dmDisplayFrequency << " MHz" << std::endl;
			float fl = (float)devMode.dmDisplayFrequency;
			UE_LOG(LogTemp, Display, TEXT("GPU Clock: %f"), fl);
		}
	}
}

void ANotFound404Character::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	// �������� ���������� � ����������
	FString CPUInfo = FGenericPlatformMisc::GetCPUBrand();
	// �������� ���������� ���� ����������
	int32 NumCores = FGenericPlatformMisc::NumberOfCores();
	// �������� ���������� ���� ���������� � ��� ���-��
	int32 NumAllCores = FGenericPlatformMisc::NumberOfCoresIncludingHyperthreads();
	// ������� ���������� � ���������� � ���
	UE_LOG(LogTemp, Display, TEXT("CPU: %s, Cores: %d, AllCores: %d"), *CPUInfo, NumCores, NumAllCores);


	/// ================ NEW CODE ==========================

	if (UGameplayStatics::DoesSaveGameExist("ServerDataSavedSlot", 0))
	{
		LoadGame();
	}
	else
	{
		CreateSaveFile();
	}


	/// ================ NEW CODE ==========================

	
	// ������ ��� �����
	// DisplayInfo();
	
	// Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

}

//////////////////////////////////////////////////////////////////////////// Input

void ANotFound404Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ANotFound404Character::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ANotFound404Character::Look);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}


void ANotFound404Character::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void ANotFound404Character::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X * MouseSensitivity * YInvertion);
		AddControllerPitchInput(LookAxisVector.Y * MouseSensitivity);
	}
	SetFieldOfView();
}

void ANotFound404Character::SetHasRifle(bool bNewHasRifle)
{
	bHasRifle = bNewHasRifle;
}

bool ANotFound404Character::GetHasRifle()
{
	return bHasRifle;
}

void ANotFound404Character::SetFieldOfView()
{
	if (FirstPersonCameraComponent)
	{
		FirstPersonCameraComponent->SetFieldOfView(FieldOfView);
	}
}


