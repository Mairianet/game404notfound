// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnterLobbyCodeWidgetClass.h"
#include "ShowLobbyCodeWidgetClass.h"
#include "Blueprint/UserWidget.h"
#include "Templates/SubclassOf.h"

#include "MainMenuWidgetClass.generated.h"


/**
 * 
 */
UCLASS()
class NOTFOUND404_API UMainMenuWidgetClass : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Widgets")
	void OpenShowLobbyCodeWidget();
	
	UFUNCTION(BlueprintCallable, Category = "Widgets")
	void OpenEnterLobbyCodeWidget();
	
protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* NewGameButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* ConnectToGameButton;
	
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnNewGameButtonClicked();

	UFUNCTION()
	void OnConnectToGameButtonClicked();
	
	UFUNCTION()
	void SendError(FString errorMsg, USaveGameData* dataToSave);

	UFUNCTION()
	void PostServerRequest(const FString& URL, const FString& Content);
	
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UShowLobbyCodeWidgetClass> ShowLobbyCodeWidgetClass;
	
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UEnterLobbyCodeWidgetClass> EnterLobbyCodeWidgetClass;
};