﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ShowLobbyCodeWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UShowLobbyCodeWidgetClass : public UUserWidget
{
	GENERATED_BODY()

public:
	UShowLobbyCodeWidgetClass(const FObjectInitializer& ObjectInitializer);

protected:
    virtual void NativeConstruct() override;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* CodeLabel;
};
