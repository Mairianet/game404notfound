// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidgetClass.h"

#include "EnterLobbyCodeWidgetClass.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "ShowLobbyCodeWidgetClass.h"
#include "HttpModule.h"
#include "SaveGameData.h"
#include "Interfaces/IHttpResponse.h"
#include "Kismet/GameplayStatics.h"
#include "JsonUtilities.h"

class UShowLobbyCode;

void UMainMenuWidgetClass::OpenShowLobbyCodeWidget()
{
	UShowLobbyCodeWidgetClass* ShowLobbyCodeWidget = CreateWidget<UShowLobbyCodeWidgetClass>(GetWorld(), ShowLobbyCodeWidgetClass);
	if (ShowLobbyCodeWidget != nullptr)
	{
		ShowLobbyCodeWidget->AddToViewport();
	}
}

void UMainMenuWidgetClass::OpenEnterLobbyCodeWidget()
{
	UEnterLobbyCodeWidgetClass* EnterLobbyCodeWidget = CreateWidget<UEnterLobbyCodeWidgetClass>(GetWorld(), EnterLobbyCodeWidgetClass);
	if (EnterLobbyCodeWidget != nullptr)
	{
		EnterLobbyCodeWidget->AddToViewport();
	}
}

void UMainMenuWidgetClass::NativeConstruct()
{
	Super::NativeConstruct();
	if (NewGameButton)
	{
		NewGameButton->OnClicked.AddDynamic(this, &UMainMenuWidgetClass::OnNewGameButtonClicked);
	}
	
	if (ConnectToGameButton)
	{
		ConnectToGameButton->OnClicked.AddDynamic(this, &UMainMenuWidgetClass::OnConnectToGameButtonClicked);
	}
}

void UMainMenuWidgetClass::OnNewGameButtonClicked()
{
	const USaveGameData* dataToLoad = Cast<USaveGameData>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	const FString UserId = dataToLoad->UserId;
	const FString PostURL = TEXT("http://localhost:8181/api/v1/servers");
	const FString Content = FString::Printf(TEXT("{ \"creatorId\": \"%s\", \"isPublic\": false }"), *UserId);

	PostServerRequest(PostURL, Content);
}

void UMainMenuWidgetClass::OnConnectToGameButtonClicked()
{
	OpenEnterLobbyCodeWidget();
}

void UMainMenuWidgetClass::SendError(FString errorMsg, USaveGameData* dataToSave)
{
	dataToSave->Code = -1;
	dataToSave->ServerId = 0;
	UGameplayStatics::SaveGameToSlot(dataToSave, "ServerDataSavedSlot", 0);
	UE_LOG(LogTemp, Error, TEXT("%s"), *errorMsg);
}

void UMainMenuWidgetClass::PostServerRequest(const FString& URL, const FString& Content)
{
    FHttpModule* Http = &FHttpModule::Get();
    UE_LOG(LogTemp, Warning, TEXT("POST METHOD"));
    USaveGameData* dataToLoad = Cast<USaveGameData>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
    dataToLoad->Code = 0;
    UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
    const TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    Request->SetURL(URL);
    Request->SetVerb("POST");
    Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
    Request->SetContentAsString(Content);

    Request->OnProcessRequestComplete().BindLambda([this, dataToLoad](FHttpRequestPtr Request, FHttpResponsePtr Response, bool bSuccess)
    {
        if (bSuccess && Response.IsValid() && (Response->GetResponseCode() == EHttpResponseCodes::Ok || Response->GetResponseCode() == EHttpResponseCodes::Created))
        {
            FString ResponseContent = Response->GetContentAsString();
            UE_LOG(LogTemp, Warning, TEXT("Response Content: %s"), *ResponseContent);

            TSharedPtr<FJsonObject> JsonObject;
            TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(ResponseContent);
            if (FJsonSerializer::Deserialize(JsonReader, JsonObject))
            {
                int32 Code;
                int32 ServerId;
                if (JsonObject->TryGetNumberField("code", Code) && JsonObject->TryGetNumberField("id", ServerId))
                {
                    dataToLoad->Code = Code;
                    dataToLoad->ServerId = ServerId;
                    UE_LOG(LogTemp, Warning, TEXT("CODE %d"), dataToLoad->Code);
                    UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
                }
                else
                {
                	SendError("Failed to parse code from JSON", dataToLoad);
                }
            }
            else
            {
            	SendError("Failed to deserialize JSON", dataToLoad);
            }
        }
        else
        {
        	SendError("Failed to process server request", dataToLoad);
        }
    	OpenShowLobbyCodeWidget();
    });

    Request->ProcessRequest();
}


