// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotFound404GameMode.h"
#include "NotFound404Character.h"
#include "UObject/ConstructorHelpers.h"

ANotFound404GameMode::ANotFound404GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
